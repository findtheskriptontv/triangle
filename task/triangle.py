def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''

    check_a = a < b+c
    check_b = b < a+c
    check_c = c < a+b

    result = False

    if check_a == True and check_b == True and check_c == True:
        result = True
        

    return result
